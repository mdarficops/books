# **CLEAN CODE**

* [Chapitre 2 : Meaningful Names](#chapitre-2-:-meaningful-names)
  - [Introduction](#introduction)
  - [Use Intention-Revealing Names](#use-intention-revealing-names)
  - [Avoid Disinformation](#avoid-disinformation)
  - [Make Meaningful Distinctions](#make-meaningful-distinctions)
  - [Use Pronounceable Names](#use-pronounceable-names)
  - [Use Searchable Names](#use-searchable-names)
  - [Hungarian Notation](#hungarian-notation)
  - [Interfaces and Implementations](#interfaces-and-implementations)
  - [Avoid Mental Mapping](#avoid-mental-mapping)
  - [Class Names](#class-names)
  - [Method Names](#method-names)
  - [Pick One Word per Concept](#pick-one-word-per-concept)
  - [Don’t Pun](#don’t-pun)
  - [Final Words](#final-words)


## **Chapitre 2 : Meaningful Names**

### **Introduction**
Les noms se retrouvent partout dans nos logiciels, on nomme les variables, les méthodes, les classes ainsi que des fichiers et des répertoires, de même que nos livrables (jar, ear, war).
Etant donné que nous nommons beaucoup de choses, nous devons bien le faire.

### **Use Intention-Revealing Names**
Le nom doit révéler l’intention, il décrit le pourquoi de la variable / classe / méthodes en question, ce qu’elle fait et comment l’utiliser.
Ce nom doit se suffire à lui même, s’il a besoin d’être commenté c’est qu’il n’est pas assez explicite et ne défini pas réellement l’intention de l’objet.
Par exemple une variable numérique d, ne nous enseigne rien. Hors si nous la nommons elapsedTimeInDays, alors elle évoque une intention beaucoup plus claire.
Un code simple de quelques lignes seulement, peut très vite devenir fastidieux à lire et comprendre si les noms ne sont pas correctement choisis.

Comparer le code suivant avec celui qui suit.

```
public List<int[]> getThem() {
	List<int[]> list1 = new ArrayList<int[]>();
	for (int[] x : theList)
		if (x[0] == 4)
			list1.add(x)
	return list1;
}
```

```
public List<int[]> getFlaggedCells() {
	List<Cell> flaggedCells = new ArrayList<Cell>();
	for (Cell cell : gameBoard)
		if (cell.isFLagged())
			flaggedCells.add(cell)
	return flaggedCells;
}
```

Les deux morceaux de code précédent font exactement la même chose, contiennent le même nombre de ligne. Le premier, bien qu’il n’y ait aucune expression complexe, est très difficile à comprendre.
Contrairement au deuxième code, qui est beaucoup plus explicite et simple à comprendre, on voit la puissance de bien nommer les choses.

### **Avoid Disinformation**
Il faut éviter de nommer par ce qui peut prêter à confusion, le nom hp peut être à première vue une bon nom si on code un hypoténuse, mais il pourrait être confondu avec un nom de la plateforme Unix.
De la même manière, combien de temps il faut pour comprendre la subtilité entre XYZControllerForEffecientHandlingOfStrings et XYZControllerForEffecientStorageOfStrings.
Un autre exemple concret et l’utilisation de la lettre « l » minuscule et « o » majuscule, qui peuvent être confondues avec les chiffres 0 et 1.

### **Make Meaningful Distinctions**
De la même manière que les noms qui peuvent prêter à confusion, les noms dénués de sens sont à bannir comme une méthode qui aurait pour argument (char[] a1, char[] a2) on comprendrait immédiatement le sens si on utilisait les mots « source » et « destination »
Certains mots parasites empêchent également de comprendre la différence entre plusieurs objets, par exemple Product, ProductInfo, ProductData.

### **Use Pronounceable Names**
Utiliser des mots facilement prononçables, utiliser « genymdhms » pour « generation date, year, month, day, hour, minute, second » n’est pas un bon choix. Ce mot est imprononçable, s’il est imprononçable alors on ne pourra pas en parler sans passer pour un idiot.
La programmation est aussi une activité sociale.

### **Use Searchable Names**
Les noms possédant une lettre ainsi que les chiffres sont difficiles à rechercher, de ce fait les lettres sont autorisées dans les boucles par exemple. Sinon, de préférence utiliser des noms facilement retrouvable, et les noms les plus long sont plus facile à retrouver que les noms courts.

### **Hungarian Notation**
Avec les langages fortement typés de nos jours, ainsi que les IDE qui permettent de détecter les erreurs bien avant la compilation, il n’est plus nécessaire que le nom mentionne le type de l’objet : String phoneString.
D’autant plus que lorsque le type changera, le nom lui pourrait ne pas changer : PhoneNumber phoneString.

### **Interfaces and Implementations**
Si on créer une Abstract Factory pour la création de figures, il nous faut une interface et une implémentation.
Les logiciels dont on hérite aujourd’hui préfixe leur interface avec « I », IShapeFactory, ce « I » est au mieux une distraction et au pire trop d’information. Il n’est pas nécessaire que les utilisateurs sachent que je leur exposent une interface.
Ainsi ShapeFactory est meilleur, l’implémentation pourrait être ShapeFactoryImp ou même l’affreux CShapeFactory est préférable que d’encoder l’interface.

### **Avoid Mental Mapping**
Il faut utiliser des noms judicieusement choisi, qui concernent la problématique et le sujet en question, de façon à préserver les lecteurs d’une traduction en des termes qu’ils connaissent.

### **Class Names**
Les classes et les objets doivent être des noms et des phrases comme : Customer, WikiPage, Account, AddressParser. Et d’éviter de contenir Manager, Processor, Data ou Info.
Un nom de class ne doit pas être un verbe.

### **Method Names**
Les méthodes devraient être des verbes ou phrases verbales comme : postPayment, deletePage ou save
Les Accessors, mutators et predicate, devraient être nommés par leur valeur et préfixé par get, set et is en accord avec les standards.
String name = employee.getName();
Customer.setName(« mike »);
if(paycheck.isPosted())…

Lorsqu’un constructeur est surchargé, on peut passer par une méthode explicite qui décrit les arguments
Complex fulcrumPoint = Complex.FromRealNumber(23.0) est généralement meilleur que Complex fulcrumPoint = new Complex(23.0);
En faisant appel à un constructeur privé derrière.

### **Pick One Word per Concept**
Rester cohérent et utiliser le même terme pour désigner la même action.
fetch, retrieve et get sont des méthodes équivalentes, ainsi que contrôler, driver, manager peuvent désigner la même chose.
Ainsi pour éviter de perdre du temps, il est conseillé d’utiliser le même terme dans l’ensemble du code.

### **Don’t Pun**
Il ne faut pas non plus utiliser un terme pour deux idées différentes, on pourrait préférer utiliser insert ou append à la place de add dans certains cas.
Le but étant que l’auteur soit le plus compréhensible et cohérent possible.

### **Final Words**
Tout ceci exige de bonnes compétences de description et un bagage culturel commun.
Il s’agit plus d’une question d’enseignement que de question technique, managériales ou autres.
Le problème étant que les gens n’apprennent pas à bien le faire.

Déplus nous avons différents outils qui nous permettent facilement de tout renommer, utilisons les.
Suivre ces quelques règles porte ces fruits sur le court et le long terme.